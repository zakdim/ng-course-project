# NgCourseProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## S03-45 - Add Bootstrap to the project

Install bootstrap locally in the project: `npm install --save bootstrap@3`

Add `bootstrap.min.css` to `angular.json`


```
  "styles": [
    "node_modules/bootstrap/dist/css/bootstrap.min.css",
    "src/styles.css"
  ],
```

## S19-279 Backend (Firebase) Setup

* Firebase:
https://console.firebase.google.com/u/0/project/ng-course-recipe-book-v2-2c265/database/ng-course-recipe-book-v2-2c265-default-rtdb/data

* Base URL:
https://ng-course-recipe-book-v2-2c265-default-rtdb.firebaseio.com/

## S20-290 Handling Form Input

###  Code Formatting

* Example HTML code

```
        <input type="email" id="email" class="form-control" ngModel name="email">
```

* Place the cursor on the and press CMD+SHIFT+F to format (using Prettier Code Formatter plugin)
```
        <input
          type="email"
          id="email"
          class="form-control"
          ngModel
          name="email"
        />
```

## S20-293 Preparing the Signup Request

### Firebase Auth REST API

* Sign up with email / password

https://firebase.google.com/docs/reference/rest/auth#section-create-email-password

Endpoint: `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]`
Web API key: `AIzaSyBNDI0G_q0HjL3rCYtDOwxbEWlkkV-Oea0`

## S20-300 Reflecting the Auth State in the UI

* Users
```
test@test.com:tester
test2@test.com:tester2
```

## S23-342 Deployment Example: Firebase Hosting

```
ng build --prod
npm install -g firebase-tools
firebase login
firebase init

? What do you want to use as your public directory? dist/ng-course-project

? Configure as a single-page app (rewrite all urls to /index.html)? (y/N) y

? File dist/ng-course-project/index.html already exists. Overwrite? (y/N) N

firebase deploy

Hosting URL: https://ng-course-recipe-book-v2-2c265.web.app
```

## S24-347 Getting Started with Reducers

```
# Install NgRx
npm install --save @ngrx/store

```

## S24-365 Expoloring NgRx Effects

```
# Install NgRx Effects
npm install --save @ngrx/effects

```

## S24-377 Using the Store Devtools

```
npm install --save-dev @ngrx/store-devtools
```

## S24-378 The Router Store

```
npm install --save @ngrx/router-store
```

## S24-385 Cleanup Work

* Copy of recipes remove with recipe.service.ts

```
  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'Tasty Schnitzel',
  //     'A super-tasty Schnitzel - just awesome!',
  //     'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG',
  //     [
  //       new Ingredient('Meat', 1),
  //       new Ingredient('French Fries', 20)
  //     ]),
  //   new Recipe(
  //     'Big Fat Burger',
  //     'What else you need to day?',
  //     'https://upload.wikimedia.org/wikipedia/commons/b/be/Burger_King_Angus_Bacon_%26_Cheese_Steak_Burger.jpg',
  //     [
  //       new Ingredient('Buns', 2),
  //       new Ingredient('Meat', 1)
  //     ]),
  //   // new Recipe(
  //   //   'Spaghetti',
  //   //   'Tasty Spaghetti!',
  //   //   'https://upload.wikimedia.org/wikipedia/commons/2/2a/Spaghetti_al_Pomodoro.JPG',
  //   //   [
  //   //     new Ingredient('Spaghetti', 10),
  //   //   ]),
  //   // new Recipe(
  //   //   'Summer Salad',
  //   //   'A juicy summer salad!',
  //   //   'https://www.maxpixel.net/static/photo/1x/Salad-Summer-Salad-Eat-Saltatteller-843085.jpg',
  //   //   []),
  // ];
```

## S25-390 Adding Angular Universal

```
ng add @nguniversal/express-engine --clientProject ng-course-project

npm install --save @nguniversal/module-map-ngfactory-loader

npm run build:ssr

npm run serve:ssr
```
